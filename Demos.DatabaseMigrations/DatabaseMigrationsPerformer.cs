﻿using System;
using System.Collections.Generic;
using System.Data;
using Demos.DemosHelpers;

namespace Demos.DatabaseMigrations
{
    internal class DatabaseMigrationsPerformer
    {
        private bool? _lastMigrationSuccessful = null;

        public void RunMigrations(MIGRATION_TYPES[] types, IDbConnection connection, DatabaseMigrationsProvider provider)
        {
            if (connection == null)
            {
                throw new ArgumentNullException();
            }

            if (connection.State != ConnectionState.Open)
            {
                throw new InvalidOperationException($"Connection needs to be in {ConnectionState.Open} state to perform migrations.");
            }

            var structs = provider.GetOrderedSqlMigrations();
            var existingRecords = provider.GetOrderedMigrationRecords(connection);

            if (structs == null || structs.Length == 0)
            {
                return;
            }

            UpdateDBStructure(connection, existingRecords, structs);

        }

        /// <summary>
        /// Creates migration table within the database
        /// </summary>
        /// <param name="connection"></param>
        public void InitializeDBStructure(IDbConnection connection)
        {
            using (var tra = connection.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    DbSql_MigrationsSetup_Transaction(connection);
                    tra.Commit();
                }
                catch (Exception)
                {
                    tra.Rollback();
                    throw;
                }
            }
            //connection.Close();
        }

        /// <summary>
        /// Updates the database by provided migrations
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="existingRecords"></param>
        /// <param name="migrations"></param>
        private void UpdateDBStructure(IDbConnection connection, DatabaseMigrationRecord[] existingRecords, DatabaseMigrationStructure[] migrations)
        {
            try
            {
                using (var tra = connection.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    try
                    {
                        DbSql_MigrationExecution_Transaction(connection, existingRecords, migrations);
                        tra.Commit();
                    }
                    catch (Exception)
                    {
                        tra.Rollback();
                        throw;
                    }
                }
                //connection.Close();
            }
            catch
            {
                throw new Exception("Updating database failed.");
            }
        }

        private void DbSql_MigrationsSetup_Transaction(IDbConnection connection)
        {
            var sql = $@"
                CREATE TABLE migrations
                (
                    id INTEGER primary key,
                    name varchar(500),
                    type varchar(255),
                    executionDate text,
                    success int(1)
                )
            ";

            UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, connection);
        }

        private void DbSql_MigrationExecution_Transaction(IDbConnection connection, IReadOnlyList<DatabaseMigrationRecord> orderedExistingRecords, IEnumerable<DatabaseMigrationStructure> orderedMigrations)
        {
            var tracker = 0;
            foreach (var dbMigration in orderedMigrations)
            {
                if (tracker < orderedExistingRecords.Count)
                {
                    var curRec = orderedExistingRecords[tracker];

                    var isMatch =
                        tracker == curRec.Id &&
                        curRec.Name == dbMigration.Name &&
                        curRec.Success;

                    if (!isMatch)
                    {
                        throw new InvalidOperationException($"No matching row found in migrations table for migration on index: {tracker}.");
                    }
                }
                else
                {
                    bool? success = null;

                    AddMigrationRecord(tracker, dbMigration, connection, false);

                    if (_lastMigrationSuccessful == false)
                    {
                        continue;
                    }

                    try
                    {
                        UnifiedDatabaseHelperClass.ExecuteNonQuery(dbMigration.Sql, connection);
                        success = true;
                    }
                    catch
                    {
                        success = false;
                    }
                    finally
                    {
                        if (success == true)
                        {
                            UpdateMigrationSuccess(connection, tracker, true);
                            _lastMigrationSuccessful = true;
                        }
                    }

                    if (success != true)
                    {
                        // not throwing exception since this happens in a transaction and the failed (success = '0') record should be preserved. At the same time, don't go any further.
                        _lastMigrationSuccessful = false;
                        return;
                    }
                }

                tracker++;
            }
        }

        private bool AddMigrationRecord(int index, DatabaseMigrationStructure migration, IDbConnection connection, bool success)
        {
            var successInt = success ? 1 : 0;

            var sql = $@"
                INSERT INTO
                 migrations (
                      id,
                      name,
                      type,
                      executionDate,
                      success
                 )
                 values (
                        '{index}',
                        '{migration.Name}',
                        '{migration.Type}',
                        '{DateTime.Now:yyyy-MM-dd HH:mm:ss}',
                        '{successInt}'
                 );
            ";

            UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, connection);

            return true;
        }

        private static void UpdateMigrationSuccess(IDbConnection connection, int Id, bool success)
        {
            var successInt = success ? 1 : 0;

            var sql = $@"
                UPDATE migrations
                SET success = '{successInt}'
                WHERE id = '{Id}';
            ";

            UnifiedDatabaseHelperClass.ExecuteNonQuery(sql, connection);
        }

    }
}
