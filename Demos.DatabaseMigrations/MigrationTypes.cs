﻿namespace Demos.DatabaseMigrations
{
    public enum MIGRATION_TYPES
    {
        UNKNOWN = 0,
        STRUCTURE,
        BASIC_DATA,
        DUMMY_DATA
    }
}
