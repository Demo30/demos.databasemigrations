﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demos.DatabaseMigrations
{
    internal class DatabaseMigrationCollector
    {
        public DatabaseMigrationStructure[] GetOrderedStructures
        {
            get
            {
                var ordered = _structures.OrderBy(str => str.DateTime);
                var structs = ordered.ToArray();
                return structs;
            }
        }

        private readonly List<DatabaseMigrationStructure> _structures = new List<DatabaseMigrationStructure>();

        public void AddMigrationsStructure(DatabaseMigrationStructure structure)
        {
            if (structure == null)
            {
                throw new ArgumentNullException(nameof(structure));
            }

            _structures.Add(structure);
        }

        public void ClearCollection()
        {
            _structures.Clear();
        }
    }
}