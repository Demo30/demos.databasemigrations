﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Demos.DatabaseMigrations
{
    public class DatabaseMigrationRecord
    {
        public MIGRATION_TYPES Type { get; set; } = MIGRATION_TYPES.UNKNOWN;
        public int Id { get; set; }
        public DateTime ExecutionDate { get; set; }
        public string Name { get; set; }
        public bool Success { get; set; }
    }
}
