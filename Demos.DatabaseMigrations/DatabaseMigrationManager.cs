﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Resources;
using System.Globalization;
using System.Data;
using Demos.DatabaseMigrations;

namespace Demos.DatabaseMigrations
{
    public abstract class DatabaseMigrationManager
    {
        private DatabaseMigrationsProvider _provider;
        private DatabaseMigrationsPerformer _performer;

        public DatabaseMigrationStructure[] LoadedMigrationsOrdered => _provider.GetOrderedSqlMigrations();

        public DatabaseMigrationManager()
        {
            _provider = new DatabaseMigrationsProvider();
            _performer = new DatabaseMigrationsPerformer();
        }

        public abstract void LoadResources();

        public abstract string GetDatabaseFileName();

        public void InitializeMigrationTable(IDbConnection connection)
        {
            _performer.InitializeDBStructure(connection);
        }

        public void UpdateMigrations(MIGRATION_TYPES[] types, IDbConnection connection)
        {
            _performer.RunMigrations(types, connection, _provider);
        }

        protected void LoadMigrationsFromResource(ResourceSet resourceSet)
        {
            _provider.ClearSqlMigrationCollection();

            foreach (DictionaryEntry entry in resourceSet)
            {
                var resourceKey = entry.Key.ToString();
                var content = entry.Value.ToString();

                var values = GetPrefixAndDate(resourceKey);

                var success = _provider.AddToCollection(values.Item2, values.Item1, resourceKey, content);

                if (success == false)
                {
                    throw new Exception($"Something went wrong. Failed at resource: \"{resourceKey}\"");
                }
            }
        }

        protected DatabaseMigrationRecord[] LoadExistingRecordsInDatabase(IDbConnection connection)
        {
            return _provider.GetOrderedMigrationRecords(connection);
        }

        #region Supportive methods

        private Tuple<MIGRATION_TYPES, DateTime> GetPrefixAndDate(string name)
        {
            var dateTime15Chars = GetDateSubstring(name);

            var convertedDate = DatabaseMigrationsProvider.ConvertToDate(dateTime15Chars);

            Tuple<MIGRATION_TYPES, DateTime> result = null;

            var type = MIGRATION_TYPES.UNKNOWN;

            if (name.IndexOf(DatabaseMigrationsProvider.STRUCT_PREFIX, StringComparison.Ordinal) != -1)
            {
                type = MIGRATION_TYPES.STRUCTURE;
            }
            else if (name.IndexOf(DatabaseMigrationsProvider.BASICDATA_PREFIX, StringComparison.Ordinal) != -1)
            {
                type = MIGRATION_TYPES.BASIC_DATA;
            }
            else if (name.IndexOf(DatabaseMigrationsProvider.DUMMYDATA_PREFIX, StringComparison.Ordinal) != -1)
            {
                type = MIGRATION_TYPES.DUMMY_DATA;
            }
            else
            {
                throw new Exception();
            }

            result = new Tuple<MIGRATION_TYPES, DateTime>(type, convertedDate);
            return result;
        }

        private string GetDateSubstring(string migrationFilename)
        {
            var argumentException = new ArgumentException();

            if (string.IsNullOrEmpty(migrationFilename))
            {
                throw argumentException;
            }

            var firstRequiredUnderscore = migrationFilename.IndexOf("_", StringComparison.Ordinal);

            if (firstRequiredUnderscore == -1)
            {
                throw argumentException;
            }

            if (migrationFilename.Length < firstRequiredUnderscore + Constants.DATE_LENGTH)
            {
                throw argumentException;
            }

            var result = migrationFilename.Substring(firstRequiredUnderscore + 1, Constants.DATE_LENGTH);

            return result;
        }

        #endregion
    }
}
