﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demos.DatabaseMigrations
{
    public class MigrationFailException : Exception
    {
        public MigrationFailException(string message) : base (message)
        {

        }
    }
}
