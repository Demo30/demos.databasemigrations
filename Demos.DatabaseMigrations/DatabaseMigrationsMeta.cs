﻿using System.Reflection;
using Demos.DemosHelpers;

namespace Demos.DatabaseMigrations
{
    public static class DatabaseMigrationsMeta
    {
        public static Versioning Version {
            get
            {
                var versionString = Assembly.GetExecutingAssembly()
                    .GetName()
                    .Version
                    .ToString();
                var versioning = new Versioning();
                versioning.LoadVersion(versionString);
                return versioning;
            }
        }
    }
}
