﻿using System;

namespace Demos.DatabaseMigrations
{
    public class DatabaseMigrationStructure
    {
        public string Name { get; set; }
        public MIGRATION_TYPES Type { get; set; } = MIGRATION_TYPES.UNKNOWN;
        public DateTime DateTime { get; set; }
        public string Sql { get; set; }

        internal DatabaseMigrationStructure()
        {

        }
    }
}
