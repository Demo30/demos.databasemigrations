﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Globalization;
using System.Data;

using Demos.DemosHelpers;

namespace Demos.DatabaseMigrations
{
    internal class DatabaseMigrationsProvider
    {
        public const string STRUCT_PREFIX = "struct_";
        public const string BASICDATA_PREFIX = "basicData_";
        public const string DUMMYDATA_PREFIX = "dummyData_";

        private int CurrentNumberOfMigrations => _collector.GetOrderedStructures.Length;

        private readonly DatabaseMigrationCollector _collector;

        public DatabaseMigrationsProvider()
        {
            _collector = new DatabaseMigrationCollector();
        }

        public DatabaseMigrationsProvider(DatabaseMigrationCollector collector) : this()
        {
            _collector = collector ?? throw new ArgumentNullException();
        }

        public void ClearSqlMigrationCollection()
        {
            _collector.ClearCollection();
        }

        public bool AddToCollection(DateTime date, MIGRATION_TYPES type, string name, string sql)
        {
            var str = new DatabaseMigrationStructure
            {
                Sql = sql,
                Type = type,
                DateTime = date,
                Name = string.IsNullOrEmpty(name) ? string.Concat(type, "_", date.ToShortDateString()) : name
            };

            _collector.AddMigrationsStructure(str);

            return true;
        }

        /// <summary>
        /// Returns chronologically ordered (starting from the past to present) sql commands as defined in SQL migration files.
        /// </summary>
        public DatabaseMigrationStructure[] GetOrderedSqlMigrations()
        {
            return _collector.GetOrderedStructures;
        }

        /// <summary>
        /// Gets migration records, ordered chronologically, already stored in the database.
        /// </summary>
        public DatabaseMigrationRecord[] GetOrderedMigrationRecords(IDbConnection connection)
        {
            var migrations = new List<DatabaseMigrationRecord>();

            var sql = @"Select * From migrations Order by id";

            var dt = UnifiedDatabaseHelperClass.GetResultsDataTable(sql, connection);

            foreach(DataRow dr in dt.Rows)
            {
                var curRec = new DatabaseMigrationRecord
                {
                    Id = dr["id"].ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException),
                    Name = dr["name"].ToString(),
                    Type = ParseDBValue(dr["type"].ToString()),
                    ExecutionDate = DateTime.ParseExact(dr["executionDate"].ToString(), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                    Success = dr["success"].ToString() == "1"
                };

                migrations.Add(curRec);
            }

            return migrations.ToArray();
        }

        public static DateTime ConvertToDate(string myFormat)
        {
            if (myFormat.Substring(8,1) != "_" || myFormat.Length != Constants.DATE_LENGTH)
            {
                throw new ArgumentException("Argument not in correct format.");
            }

            var res = DateTime.ParseExact(myFormat, "yyyyMMdd_HHmmss", CultureInfo.InvariantCulture);
            return res;
        }

        private MIGRATION_TYPES ParseDBValue(string databaseValue)
        {
            switch (databaseValue)
            {
                case nameof(MIGRATION_TYPES.STRUCTURE): return MIGRATION_TYPES.STRUCTURE;
                case nameof(MIGRATION_TYPES.BASIC_DATA): return MIGRATION_TYPES.BASIC_DATA;
                case nameof(MIGRATION_TYPES.DUMMY_DATA): return MIGRATION_TYPES.DUMMY_DATA;
                default:
                    throw new ArgumentException($"Provided value \"{databaseValue}\" could not be parsed as one of the {nameof(MIGRATION_TYPES)} values.");
            }
        }
    }
}

